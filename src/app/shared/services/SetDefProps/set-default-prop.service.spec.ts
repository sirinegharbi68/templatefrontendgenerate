import { TestBed } from '@angular/core/testing';

import { SetDefaultPropService } from './set-default-prop.service';

describe('SetDefaultPropService', () => {
  let service: SetDefaultPropService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SetDefaultPropService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
