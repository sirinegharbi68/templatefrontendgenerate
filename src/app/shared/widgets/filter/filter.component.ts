import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from "@angular/core";
import {AppTranslateService} from "@shared/services";
import {FormBuilder, FormControl, FormGroupDirective, UntypedFormControl, UntypedFormGroup} from "@angular/forms";
import {COMMON_Filter_TYPES} from "@shared/constantes/Constante";
import {isEmptyValue} from "@shared/tools";
import {CriteriaSearch, Pagination, RequestObject, SearchObject} from "@shared/models";
import {onSelectFilter} from "@shared/tools/utils/filter-factory";
import {ConstanteWs} from "@shared/constantes/ConstanteWs";
import {SharedService} from "@shared/services/sharedWs/shared.service";
import {Observable} from "rxjs";

@Component({
    selector: "st2i-filter",
    templateUrl: "./filter.component.html",
    styleUrls: ["./filter.component.css"]
})
export class FilterComponent implements OnInit {
    @Input() metadata: any;
    showMoreFilterOptions: boolean = true;
    @ViewChild(FormGroupDirective) formDirective: FormGroupDirective;
    @Output() onSelectFilter = new EventEmitter<any>();
    @Output() searchoptions = new EventEmitter<Partial<any>>();
    searchObject: SearchObject;
    @Input() SelectMap: Map<string, any[]> = new Map();
    form: UntypedFormGroup;
    COMMON_Filter_TYPES = COMMON_Filter_TYPES;
    params: any = [];
    @Output() onSelecTables = new EventEmitter<any>();


    constructor(private appTranslateService: AppTranslateService,
                private fb: FormBuilder, private sharedService: SharedService
    ) {
        this.initSearchObject()
    }

    ngOnInit(): void {
        this.searchObject = this.initSearchObject();
        this.cardMetadata(this.metadata);
        this.initForm();
        this.getTables()


    }

    /**Init Search Object*/
    initSearchObject() {
        const searchObject = new SearchObject();
        searchObject.pagination = new Pagination(0, 10);
        console.log(searchObject);
        return searchObject;
    }

    /**Return field based on lang*/
    getFieldKey(field): string {
        if (typeof field == "object") {
            return field[this.appTranslateService.getDefaultLang()];
        }
        return field;
    }



    /**Init Filter Form*/
    initForm(formData?) {
        this.params["form"] = this.metadata.fields;
        this.form = this.fb.group({});
        if (isEmptyValue(formData)) {

            for (const control of this.params["form"]) {
                this.form.addControl(this.getFieldKey(control.key), new FormControl);

            }

            console.log(this.form);


        }
    }


    /**Get the control of the field based on lang*/
    getFormControl(key: string) {
        return this.form.get(this.getFieldKey(key)) as UntypedFormControl;
    }


    /**Init Card metadata Info*/
    cardMetadata(metadata) {
        return {
            title: metadata.title,
            forFilter: true

        };
    }


    /**On Search Function*/
    onSearch() {
        const searchvalue = [];
        this.params["form"].forEach((control: any) => {
                let creteriaS;
                let keySearch;
                if (isEmptyValue(control.criteriaSearch) || isEmptyValue(control.criteriaSearch?.key)) {
                    keySearch = this.getFieldKey(control.key);
                    creteriaS = this.getFieldKey(control.key);

                } else {
                    keySearch = this.getFieldKey(control.key);
                    creteriaS = this.getFieldKey(control.criteriaSearch.key);
                }
                console.log(keySearch);
                const specificSearch = isEmptyValue(control.criteriaSearch) || isEmptyValue(control.criteriaSearch.specificSearch) ? "upper_like" : control.criteriaSearch.specificSearch;
                if (!isEmptyValue(this.form.get(keySearch).value)) {
                    searchvalue.push(new CriteriaSearch(creteriaS, this.form.get(keySearch).value, specificSearch));
                }
                this.searchoptions.emit(searchvalue);

                console.log(searchvalue);

            }
        );


    }


    /**Reset Form*/
    onReset() {
        this.form.reset();
        this.searchoptions.emit(null);
        this.formDirective.resetForm();


    }


    /**Handle On Filter Select*/
    onSelectFilterEventEmitter(handler: string) {
        this.onSelectFilter.emit({handler: handler + this.metadata.ref})
        return [];
    }

    getTables(){
        let tables=[]
        for(let field of this.params["form"] ){
            if(field["type"]=="select")
            {
                tables.push(field["table"])

            }

        }
        this.onSelecTables.emit(tables)

    }


}
