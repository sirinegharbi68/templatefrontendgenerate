import {Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';
import {FormGroup, UntypedFormControl} from "@angular/forms";
import {isEmptyValue, isSomeInputsChanged} from "@shared/tools";
import {AppTranslateService} from "@shared/services";
import {Subscription} from "rxjs";

@Component({
  selector: 'st2i-select-date-range',
  templateUrl: './select-date-range.component.html',
  styleUrls: ['./select-date-range.component.css']
})
export class SelectDateRangeComponent implements OnChanges {

  // @Input() control: UntypedFormControl;
  @Input() control: FormGroup;
  @Input() label: string;
  @Input() compareTo: {start: Date, end: Date};
  @Input() min?: any;
  @Input() max?: any;

  @Input() hideRequiredMarker = false;
  @Input() hasTooltip = true;
  @Input() hint: string;


  @Output() onValueChangeEvent  = new EventEmitter<any>();

  subscription: Subscription;

  protected readonly AppTranslateService = AppTranslateService;
  protected readonly isEmptyValue = isEmptyValue;


  // form: FormGroup = new FormGroup({
  //   start: new FormControl(null),
  //   end: new FormControl(null),
  // });

  ngOnInit() {
   // this.subscription =  this.form.valueChanges.subscribe(value => {
   //    this.control.setValue({
   //      start: !isEmptyValue(value.start) ? new Date(value.start) : null,
   //      end: !isEmptyValue(value.end) ? new Date(value.end) : null
   //    });
   //    this.onValueChangeEvent.emit({
   //      start: !isEmptyValue(value.start) ? new Date(value.start) : null,
   //      end: !isEmptyValue(value.end) ? new Date(value.end) : null
   //    });
   //  })
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (isSomeInputsChanged(changes, ['min', 'max'])) {
      if (changes.min?.currentValue !== changes.min?.previousValue) {
        //this.min = new Date(changes.min.currentValue);
        this.min = !isEmptyValue(changes.min?.currentValue)? new Date(changes.min?.currentValue) : null;
      }
      if (changes.max?.currentValue !== changes.max?.previousValue) {
        // this.max = new Date(changes.max.currentValue);
        this.max = !isEmptyValue(changes.max?.currentValue)? new Date(changes.max?.currentValue) : null;
      }
    }
  }

  ngOnDestroy() {
    if (this.subscription != null) {
      this.subscription.unsubscribe();
      this.subscription = null;
    }
  }

  get tooltipTxt() {
    return null;
    // hasTooltip && (!isEmptyValue(form.value.start)||isEmptyValue(form.value.end))?(form.value.start ):(label|translate)
  }

  getFormControl(key: string){
    return this.control.get(key) as UntypedFormControl;
  }
}
